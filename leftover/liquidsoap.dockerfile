# LS build container
FROM alpine:3.12 as ls-build

RUN apk --no-cache add \
    opam \
    make \
    m4 \
    ocaml-compiler-libs \
    musl-dev \
    automake \
    autoconf \
    pkgconf \
    pcre-dev \
    libvorbis-dev \
    lame-dev \
    libmad-dev \
    opus-dev \
    fdk-aac-dev

RUN addgroup -S appgroup && adduser -S appuser -G appgroup
USER appuser

RUN opam init --disable-sandboxing
RUN test -r /appuser/.opam/opam-init/init.sh && . /appuser/.opam/opam-init/init.sh > /dev/null 2> /dev/null || true

RUN opam update && \
    opam install -y \
        liquidsoap \
        vorbis \
        lame \
        mad \
        opus \
        cry \
        fdkaac


# LS container
FROM alpine:3.12 as liquidsoap

RUN apk --no-cache add \
    pcre \
    lame \
    libmad \
    opus \
    libvorbis \
    fdk-aac

RUN addgroup -S appgroup && adduser -S appuser -G appgroup
USER appuser

COPY --from=ls-build /home/appuser/.opam /home/appuser/.opam

WORKDIR /app
COPY notlive.ogg /app/notlive.ogg
COPY playlist.liq .

CMD ["/home/appuser/.opam/default/bin/liquidsoap", "--debug-errors", "playlist.liq"]
