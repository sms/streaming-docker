# streaming-docker

sort-of minimal setup with nginx-rtmp + liquidsoap, uses traefik for ssl
# To use:
- Modify service files to include needed variables.
- ```systemctl daemon-reload && systemctl restart nginx-ssl.service && systemctl restart leftover.service```


# Test with:

docker-compose up --build
